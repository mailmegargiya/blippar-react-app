import React from 'react';

const NoResults = (props) => {
  return (
    <header className="no-results">
      <h4>{props.message}</h4>
    </header>
  );
}

export default NoResults;