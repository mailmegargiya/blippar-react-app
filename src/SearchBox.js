import React from 'react';

const SearchBox = ({ searchValue }) => {
  return (
    <section className="search-box">
      <input type="text" onChange={e => searchValue(e.target.value)} placeholder="Search Here" />
    </section>
  );
}

export default SearchBox;

