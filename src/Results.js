import React from 'react';

const Results = (props) => {
  const { searchVal, results } = props;
  const highlightResult = (string, key) => {
    let index = string.toLowerCase().indexOf(searchVal);
    if (index >= 0) {
      string = '<div>' + string.substring(0, index) +
        '<span class="highlight">' +
        string.substring(index, index + searchVal.length) +
        '</span>' + string.substring(index + searchVal.length) +
        '</div>';
    }
    return (
      <li key={key} dangerouslySetInnerHTML={{ __html: string }}></li>
    )
  }

  return (
    <section className="results">
      <h4>Results</h4>
      <ol>
        {results && results.map((item, key) => (
          highlightResult(item, key)
        ))}
      </ol>
    </section>
  );
}

export default Results;