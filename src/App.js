import React, { useEffect, useState } from 'react';
import './App.css';
import Header from './Header';
import SearchBox from './SearchBox';
import NoResults from './NoResults';
import Results from './Results';

const App = () => {
  const [apiData, setApiData] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [searchedData, setSearchedData] = useState([]);
  const [noDataMsg, setNoDataMsg] = useState("");
  const [searchVal, setSearchVal] = useState("");

  // JSON Data
  async function fetchData() {
    const response = await fetch('https://gist.githubusercontent.com/abhijit-paul-blippar/0f97bb6626cfa9d8989c7199f7c66c5b/raw/dcff66021fba04ee8d3c6b23a3247fb5d56ae3e5/words');
    // waits until the request completes...
    let data = await response.text();
    if (data) {
      setIsLoading(false)
    } else {
      setIsLoading(true)
    }
    data = data.split('\n')
    setApiData(data);
  }

  useEffect(() => {
    fetchData();
  }, []);

  const searchData = (val) => {
    setSearchVal(val);
    let data = [...apiData];
    if (val.length > 2) {
      data = data.filter(c => c.toLowerCase().includes(val.toLowerCase()))
      if (data.length < 1) {
        setNoDataMsg("No matching results found.")
      }
      setSearchedData(data);
    } else {
      setNoDataMsg("Please type at least 3 characters.")
      setSearchedData([]);
    }
  }

  return (
    <>
      <div className="search-section">
        <Header userName="Hira Gargiya" />
        <SearchBox searchValue={(val) => searchData(val)} />
      </div>
      <div className="result-section">
        {!searchedData.length ?
          <NoResults message={noDataMsg} />
          :
          <Results searchVal={searchVal} results={searchedData && searchedData} />
        }
        {isLoading &&
          "Loading..."
        }
      </div>
    </>
  );
}

export default App;
